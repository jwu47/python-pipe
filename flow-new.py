import json, os, sys
import requests
from git import Repo
import shutil, re
import fileinput
import codecs
from tempfile import mkstemp
from shutil import move
from os import remove

# helper
dbquote = '"'

def file_line_replace(file, old, new):
    for line in fileinput.FileInput(file, inplace=1):
        if  old in line:
            line = re.sub(old, new, line.strip())
        sys.stdout.write(line)

def replace(source_file_path, pattern, substring):
    fh, target_file_path = mkstemp()
    with codecs.open(target_file_path, 'w', 'utf-8') as target_file:
        with codecs.open(source_file_path, 'r', 'utf-8') as source_file:
            for line in source_file:
                target_file.write(line.replace(pattern, substring))
    remove(source_file_path)
    move(target_file_path, source_file_path)

# Step 0. load config
config = json.load(open('config.json'))

repo_name = config["repo_name"]
sln_name = config["sln"]
branch = config["branch"]
src_proj_name = config["src_proj_name"]
unit_test_proj_name = src_proj_name + ".Test.Unit"
int_test_proj_name = src_proj_name + ".Test.Integration"
template_uri = config['template_uri']
use_libs = config["use_libs"]
proj_namespace = config["proj_namespace"]


# you secret
secret = json.load(open('secret.json'))
username = secret["repo_username"]
password = secret["repo_password"]


# step 1, local dir
if not os.path.exists(repo_name):
    os.makedirs(repo_name)
else:
    print ("error: local directory exists: " +  repo_name )
    sys.exit()



# step 2 -  clone the template into local repo
Repo.clone_from(
    'https://' + username + ':' + password + ":" + template_uri,
    repo_name,
    branch='master'
)


# step 3 walk the local repo to remove .git and files
dir = os.getcwd()
repodir = dir + '/' + repo_name


for subdir, dirs, files in os.walk(repodir):
    for file in files:
       # fpath (full), fName (no path), bareName (no suffix)
       fpath = os.path.join(subdir, file)
       fname = os.path.basename(fpath)


       if (".git" not in fpath) :
          if os.path.isfile(fpath) :
            filenamebare = os.path.splitext(fname)[0]

            # rename .sln file
            if "@repo_name@" in fpath :
                 dirname = os.path.dirname(os.path.abspath(file))
                 newfname = os.path.join(repodir, re.sub("@repo_name@", repo_name, fname))
                 os.rename(fpath,  newfname)
                 ##replace projects later

       else:
           # remove .git
           os.remove(fpath)


# step 4 work on src and test
src_dir = os.path.join(repo_name, "src")
src_proj_dir = os.path.join(src_dir, src_proj_name)
test_dir = os.path.join(repo_name, "test")
unit_test_proj_dir = os.path.join(test_dir, unit_test_proj_name)
int_test_proj_dir = os.path.join(test_dir, int_test_proj_name)

# step 4.1 TBD -   rename/move of files with @xxx@,
# TBD - fun with listdir() recursively for dir @XXX@_
shutil.move(os.path.join(src_dir, "@src_project@"), src_proj_dir)
shutil.move(os.path.join(test_dir, "@unit_test_project@"), unit_test_proj_dir)
shutil.move(os.path.join(test_dir, "@int_test_project@"), int_test_proj_dir)

os.rename(os.path.join(src_proj_dir, "@src_project@" + ".csproj"),  os.path.join(src_proj_dir, src_proj_name + ".csproj"))
os.rename(os.path.join(unit_test_proj_dir, "@unit_test_project@" + ".csproj"),  os.path.join(unit_test_proj_dir, unit_test_proj_name + ".csproj"))
os.rename(os.path.join(int_test_proj_dir, "@int_test_project@" + ".csproj"),  os.path.join(int_test_proj_dir, int_test_proj_name + ".csproj"))

#SKIP -- step 4.2 take care of project.csproj with ref lib.
#libref = '  <ItemGroup>' + os.linesep
#for i in range (len(use_libs)):
#    libref += "    <PackageReference Include=" + dbquote + use_libs[i]['lib'] + dbquote + " Version=" + dbquote + use_libs[i]['version'] + dbquote + " />" + os.linesep

#libref += "  </ItemGroup>" + os.linesep

#for line in fileinput.FileInput(os.path.join(src_proj_dir, src_proj_name + ".csproj"), inplace=1):
#        if line.strip() == "@us_libs@":
#            line = line.replace(line, libref)
#        sys.stdout.writeline(line)

#step 4.3 source code namespace
for subdir, dirs, files in os.walk(repodir):
    for file in files:
        filepath = (os.path.realpath( subdir + os.sep +  file))
        if file.endswith(".cs"):
            replace(filepath, "@proj_namespace@",   proj_namespace)
        if file.endswith(".json"):
            replace(filepath, "@repo_name@",   repo_name)
        if file.endswith(".sln") or file.endswith(".csproj"):
            replace(filepath, "@src_project@",   src_proj_name)
            replace(filepath, "@int_test_project@",   int_test_proj_name)
            replace(filepath, "@unit_test_project@",   unit_test_proj_name)


# Step 5. final -  push to the remote repo

# Step 5.1
repo_url = 'https://api.bitbucket.org/2.0/repositories/jwu47/' + repo_name
post_headers = {'Content-Type': 'application/x-www-form-urlencoded'}
requests.post(repo_url, headers=post_headers, auth=(username, password))

print("BitBucket repo created: " + repo_url)

for subdir, dirs, files in os.walk(repodir):
    for file in files:

        if os.path.isfile(subdir + os.sep + file):
             dirname = subdir[len(repodir):] + "/" + file
             files = { dirname : (subdir + "/" + file, open(subdir + os.sep + file, 'rb'), 'application/multipart/form-data', {'Expires': '0'})}
             response = requests.post(repo_url + "/src",  auth=(username, password), files=files)

print("Generated files uploaded to BitBucket")