import json, os, sys
import requests
from git import Repo
import shutil, re
import fileinput
import codecs
from tempfile import mkstemp
from shutil import move
from os import remove

# helper
dbquote = '"'

## not used
def file_line_replace(file, old, new):
    for line in fileinput.FileInput(file, inplace=1):
        if  old in line:
            line = re.sub(old, new, line.strip())
        sys.stdout.write(line)

## not used
def replace(source_file_path, pattern, substring):
    fh, target_file_path = mkstemp()
    with codecs.open(target_file_path, 'w', 'utf-8') as target_file:
        with codecs.open(source_file_path, 'r', 'utf-8') as source_file:
            for line in source_file:
                target_file.write(line.replace(pattern, substring))
    remove(source_file_path)
    move(target_file_path, source_file_path)

## one-off set up for Docker kafka image
def setup_docker():
    os.system("export DOCKER_HOST=tcp://192.168.99.100:2376")
    os.system("export DOCKER_MACHINE_NAME=confluent")
    os.system("export SSH_AUTH_SOCK=/private/tmp/com.apple.launchd.cek5NQwKdN/Listeners")
    os.system("export DOCKER_TLS_VERIFY=1")
    os.system('export DOCKER_CERT_PATH=/Users/JWu/.docker/machine/machines/confluent')
    os.system('export SHLVL=1')


## uese - topic is a dict from the Topic json defintion. This uses Docker specific command
## but we can easily change that to use the command from Kafka Console
def create_topic(topic):
    create_cmd = ('docker run --net=host --rm confluentinc/cp-kafka:4.0.0 ' +
                 'kafka-topics --create --topic '+ topic['topic_name'] +
                ' --partitions ' + str(topic['partitions']) + ' --replication-factor ' +
                  str(topic['replication_factor']) + '  --if-not-exists --zookeeper localhost:32181')
    print(create_cmd)
    os.system(create_cmd)


    #assume local access to the Kafka console.



# Step 0. load config
config = json.load(open('kafkaparse-config.json'))

repo_name = config["repo_name"]
print(repo_name)
branch = config["branch"]
template_uri = config["template_uri"]

# 0.5 your secret
secret = json.load(open('secret.json'))
username = secret["repo_username"]
password = secret["repo_password"]



# step 1, local dir,
# for now - override local dir
if os.path.exists(repo_name):
   print ("info: local directory refreshed from BitBucket: " +  repo_name )
   shutil.rmtree(repo_name)

os.makedirs(repo_name)



# step 2 -  clone the template into local repo
Repo.clone_from(
    'https://' + username + ':' + password + ":" + template_uri,
    repo_name,
    branch='master'
)

# Step 3 loop thru the files and do damage
dir = os.getcwd()
repodir = dir + '/' + repo_name

for subdir, dirs, files in os.walk(repodir):
    for file in files:
        fpath = os.path.join(subdir, file)
        fname = os.path.basename(fpath)

        if (".json"  in fpath) :
            print(os.path.join(repo_name, fname))
            topic_array = json.load(open(os.path.join(repo_name,fname )))
            for topic in topic_array:
                topic_obj = topic['topic']
                topic_name = topic_obj['topic_name']
                topic_env =  topic_obj['environment']
                print('Kafka topic to create: ' + topic_name)

                 #enforcing name convetions
                if (topic_name.endswith(topic_env) and ((topic_env == 'prd') or (topic_env =='stg'))):
                    create_topic(topic_obj)
                else:
                    print('Topic name error: topic name suffix needs to match environment: ' + topic_name)
                    print('Quit the process!')

                    sys.exit()

        # we assume all '.json' is for topics!
        # we assume all '.json' is for topics!
 #       if (".json"  in fpath) :
 #           with open (fpath) as topic_file:
 #               topic_file = json.load(topic_file)
 #               for line in topic_file['topic']:
 #                   print('partitions: ' + line['partitions'])


# 3.1 parse the json file, enforce env/name ,
# TBD:  add topic name length check, special char check...


# 3.2 check required elements


# 3.3 let's create it, and if topic exists, log and keep going.


##Step 4. done