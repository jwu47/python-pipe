import json, os, sys
import requests
from git import Repo
import shutil, re
import fileinput
import codecs
from tempfile import mkstemp
from shutil import move
from os import remove

# helper
dbquote = '"'

def file_line_replace(file, old, new):
    for line in fileinput.FileInput(file, inplace=1):
        if  old in line:
            line = re.sub(old, new, line.strip())
        sys.stdout.write(line)

def replace(source_file_path, pattern, substring):
    fh, target_file_path = mkstemp()
    with codecs.open(target_file_path, 'w', 'utf-8') as target_file:
        with codecs.open(source_file_path, 'r', 'utf-8') as source_file:
            for line in source_file:
                target_file.write(line.replace(pattern, substring))
    remove(source_file_path)
    move(target_file_path, source_file_path)

# Step 0. load config
config = json.load(open('config.json'))

repo_name = config["repo_name"]
sln_name = config["sln"]
branch = config["branch"]
src_proj_name = config["src_proj_name"]
unit_test_proj_name = src_proj_name + ".Test.Unit"
int_test_proj_name = src_proj_name + ".Test.Integration"
template_uri = config['template_uri']
use_libs = config["use_libs"]
proj_namespace = config["proj_namespace"]


# you secret
secret = json.load(open('secret.json'))
username = secret["repo_username"]
password = secret["repo_password"]


# step 1, local dir
if not os.path.exists(repo_name):
    os.makedirs(repo_name)
else:
    print ("error: local directory exists: " +  repo_name )
    sys.exit()



# step 2 -  clone the template into local repo
Repo.clone_from(
    'https://' + username + ':' + password + ":" + template_uri,
    repo_name,
    branch='master'
)



# Step 5. final -  push to the remote repo

# Step 5.1
repo_url = 'https://api.bitbucket.org/2.0/repositories/jwu47/' + repo_name
post_headers = {'Content-Type': 'application/x-www-form-urlencoded'}
requests.post(repo_url, headers=post_headers, auth=(username, password))

print("BitBucket repo created: " + repo_url)

for subdir, dirs, files in os.walk(repodir):
    for file in files:

        if os.path.isfile(subdir + os.sep + file):
             dirname = subdir[len(repodir):] + "/" + file
             files = { dirname : (subdir + "/" + file, open(subdir + os.sep + file, 'rb'), 'application/multipart/form-data', {'Expires': '0'})}
             response = requests.post(repo_url + "/src",  auth=(username, password), files=files)

print("Generated files uploaded to BitBucket")